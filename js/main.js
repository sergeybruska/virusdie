$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    navText: [],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:2
        }
    }
});

$('#mentioned a').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
    var mention = $(this)
    $('#mentioned a').removeClass('active')
    mention.addClass('active')
});

$('#multiPlan li').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
    var planlink = $(this)
    $('#multiPlan a').removeClass('active')
    planlink.addClass('active')
});

$('#multiPlanMobile li').on('click', function (e) {
    e.preventDefault()
    $(this).tab('show')
    var planlink = $(this)
    $('#multiPlanMobile a').removeClass('active')
    planlink.addClass('active')
});

$('.plans-mobile__more-btn').on('click', function (e) {
    e.preventDefault()
    $('.plans-mobile__more-list').toggleClass('show')
    $('.plans-mobile__more-btn').toggleClass('active')
});

$('.navbar-toggler').on('click', function (e) {
    e.preventDefault()
    $('.navbar-toggler-icon').toggleClass('active')
});

$(function(){
    $(window).scroll(function() {
        if($(this).scrollTop() >= 100) {
            $('header').addClass(' sticky-active ', {duration:300});
        }
        else{
            $('header').removeClass(' sticky-active ', {duration:300});
        }
    });
});